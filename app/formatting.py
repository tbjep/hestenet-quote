import discord

helpMessage = discord.Embed(title="Hjælp side Hestenet Botten", color=0x00FF00)
helpMessage.set_thumbnail(
    url="https://gitlab.com/personligt-lort/hestenet-quote/-/raw/master/logo.png"
)
helpMessage.add_field(name="For en tilfældig post", value="!hest", inline=False)
helpMessage.add_field(
    name="For en tilfældig post med kommentare", value="!hest kommentar", inline=False
)
helpMessage.add_field(
    name="Få hjælp (i form af denne boks)", value="!hest help", inline=False
)
helpMessage.set_footer(text='Af bertie#5137 - for hjælp "!hest hjælp"')


async def formatPost(post):
    OP = post["OP"]

    currentMessage = discord.Embed(title=OP["title"], url=post["url"], color=0xFF6363)
    currentMessage.add_field(name="Post:", value=OP["content"])
    currentMessage.add_field(
        name="Details: ",
        value="**Author:** " + OP["author"] + "\n" + "**Date** " + OP["date"],
    )

    if "comments" in post:
        currentMessage.add_field(
            name="\u200b", value="\n---\u200b\n\u200b", inline=False
        )
        for i, comment in enumerate(post["comments"]):
            if len(currentMessage) > 4800:
                currentMessage.add_field(
                    name="Flere kommentare online...",
                    value=f"De sidste {len(post['comments']) - i} kommentare findes online.",
                )
                break

            currentMessage.add_field(
                name=f"{comment['author']}:", value=comment["content"], inline=False
            )

    currentMessage.set_footer(text='Af bertie#5137 - for hjælp "!hest hjælp"')
    return currentMessage


async def formatPostForReading(post):
    formattedPost = [
        post["OP"]["author"] + " siger: " + post["OP"]["title"],
        post["OP"]["content"],
    ]

    if "comments" in post:
        for comment in post["comments"]:
            formattedPost.append(comment["author"] + " svarer: " + comment["content"])

    return formattedPost
