#!/usr/bin/python3

import discord
from pathlib import Path

from app.scraping import getPostContents
from app.formatting import formatPost, helpMessage, formatPostForReading
from app.tts import readMultiple

import logging

import re

logging.basicConfig(level=logging.DEBUG)

token = Path("./token.secret").read_text()

client = discord.Client()


@client.event
async def on_ready():
    logging.info(f'We have logged in as "{client.user}"')


@client.event
async def on_message(message):
    logging.info(f'"{message.author}" initiates using command: "{message.content}"')

    msg = message.content.lower()
    if msg.startswith("!hest") and message.author != client.user:

        if msg.startswith("!hest hjælp"):
            await message.channel.send(embed=helpMessage)
            return

        try:
            urlEnd = re.search("(?P<url>https://[^\s]+)", message.content).group("url")
            url = "https://www.heste-nettet.dk/" + urlEnd.replace("https://www.heste-nettet.dk/", "")
            post = await getPostContents(getComments = ("kommentar" in msg), postURL=url)
        except AttributeError:
            post = await getPostContents(getComments = ("kommentar" in msg))
            

        if msg.startswith("!hest læs"):
            if message.author.voice and message.author.voice.channel:
                embedMessage = await formatPost(post)
                await message.channel.send(embed=embedMessage)

                formattedPost = await formatPostForReading(post)

                await readMultiple(message, formattedPost)

        else:
            embedMessage = await formatPost(post)
            await message.channel.send(embed=embedMessage)

    return


if __name__ == "__main__":
    client.run(token)
