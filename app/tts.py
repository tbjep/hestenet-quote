import discord

from asyncio import sleep
from gtts import gTTS
import uuid
import os

import logging


async def readMultiple(origMSG, messages):
    logging.debug("Initiating voice client")
    voiceClient = await origMSG.author.voice.channel.connect()

    for message in messages:
        if isinstance(message, int):
            await sleep(message)
        else:
            await readOut(voiceClient, message)

    logging.debug("Disconnecting voice client")

    await voiceClient.disconnect()
    voiceClient.cleanup()


async def readOut(voiceClient, TTSContents):
    fileName = str(uuid.uuid4()) + ".mp3"

    logging.debug(f'Writting message to file "{fileName}"')

    gTTS(text=TTSContents, lang="da", slow=False).save(fileName)

    logging.debug(f'Reading message from file "{fileName}"')

    voiceClient.play(discord.FFmpegPCMAudio(source=fileName))

    while voiceClient.is_playing():
        await sleep(1)

    logging.debug(f'Removing file with name "{fileName}"')

    os.remove(fileName)
